Elasticalert MLC
====

A revised version of the Yelp's Elastalert with enforced client ssl cert verification

#What's changed

Please see
https://github.com/Yelp/elastalert/pull/673/files

> Changes are made against Elasticsearch release 0.1.8 ONLY

#Why this change

To cope with the security requirements from OpenShift ootb Elasticsearch which requires client cert verification to communicate

additional three configs are always required in above case

    ca_certs: path/to/client_ca
    client_cert: path/to/client_cert
    client_key: path/to/client_key

#Packaging

Simply compress the repo as a zip file and distribute. TODO: makefile to automate the zipping